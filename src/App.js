import React from 'react';
import './App.css';
import Layout from './containers/Layout/Layout';
import Background from './components/UI/Background/Background';

function App() {
  return (
    <div className="App">
      <Background />
      <Layout />
    </div>
  );
}

export default App;
