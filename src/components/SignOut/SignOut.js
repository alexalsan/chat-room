import React from 'react';
import classes from './SignOut.module.css';

const SignOut = (props) => {
    return (
        props.auth.currentUser &&
        (   
            <div className={classes.ParentDiv}>
                <p className={classes.Info}>Connected as: <strong>{props.auth.currentUser.displayName}</strong></p>
                <button className="secondary" onClick={() => props.auth.signOut()}>SIGN OUT</button>
            </div>
        ) 
    );
};

export default SignOut;