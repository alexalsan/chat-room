import React from 'react';
import classes from './Background.module.css';

const Background = () => <div className={classes.Background}></div>;

export default Background;