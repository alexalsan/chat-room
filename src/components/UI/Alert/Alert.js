import React from 'react';
import classes from './Alert.module.css';

const Alert = (props) => {
    return (
        <div className={classes.Alert}>
            <p>{props.text}</p>
            <button className="secondary" onClick={props.clickConfirm}>{props.btnConfirm}</button>
            <button className="secondary" onClick={props.clickCancel}>{props.btnCancel}</button>
        </div>
    );
};

export default Alert;