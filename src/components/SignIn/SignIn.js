import React from 'react';
import firebase from 'firebase';
import classes from './SignIn.module.css';

const SignIn = (props) => {
    const signInGoogle = () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        props.auth.signInWithPopup(provider);
    };

    return (
        <div className={classes.ParentDiv}>
            <div className={classes.Banner}>
                <p>You are not connected</p>
                <button className="secondary" onClick={signInGoogle}>SIGN IN</button>
            </div>
            <div className={classes.Welcome}>
                <div>
                    <h1>Welcome to ChatRoom!</h1>
                    <p>Project developed by Alex Almagro</p>
                    <button className="secondary_light">
                        <a href="https://www.linkedin.com/in/alejandro-almagro-santos-353a60146/" target="_blank" rel="noopener noreferrer">
                            Check me on Linkedn
                        </a>
                    </button>
                    <button className="secondary_light">
                        <a href="https://bitbucket.org/alexalsan/chat-room/src/master/" target="_blank" rel="noopener noreferrer">
                            Check the code for this project
                        </a>
                    </button>
                </div>
            </div>
        </div>
    );
};

export default SignIn;