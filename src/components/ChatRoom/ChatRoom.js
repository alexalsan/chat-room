import React, { useRef, useState, useEffect } from 'react';
import classes from './ChatRoom.module.css';
import SignOut from '../SignOut/SignOut';
import ChatMessage from './ChatMessage/ChatMessage';
import MessageForm from './MessageForm/MessageForm';
import Loader from '../UI/Loader/Loader';
import Alert from '../UI/Alert/Alert';

import { formatDeletedMsg } from '../../utils';

import { useCollectionData } from 'react-firebase-hooks/firestore';

const ChatRoom = (props) => {
    const messagesRef = props.firestore.collection('messages');
    const query = messagesRef.orderBy('createdAt'); //.limit(25)
    const [messages] = useCollectionData(query, {idField: 'id'});

    const bottom = useRef();

    // const [showAlert, setShowAlert] = useState(false);
    const [deleteMsg, setDeleteMsg] = useState(null);

    const deleteDoc = (id) => {
        const docRef = messages ? messagesRef.doc(id) : null;
        if (docRef){
            //We delete the document
            docRef.delete()
            .then(function() {
                console.log("Document successfully deleted!");
            }).catch(function(error) {
                console.error("Error removing document: ", error);
            }).finally(() => { setDeleteMsg(null); });
        }
    }

    useEffect(() => {
        bottom.current.scrollIntoView({behaviour: 'smooth'});
    },[messages]);

    return (
        <div className={classes.ParentDiv}>
            <SignOut auth={props.auth}/>
            <div className={classes.Messages}>
                { deleteMsg &&
                    <Alert
                        text={`Are you sure you want to delete the message ${formatDeletedMsg(deleteMsg.text)}?`}
                        btnConfirm="Confirm" clickConfirm={() => deleteDoc(deleteMsg.id)}
                        btnCancel="Cancel" clickCancel={() => setDeleteMsg(null)}
                    />
                }
                
                {
                    messages ?
                        messages.map( (msg, i, arr) =>{
                            // const first_msg = (i===0 || arr[i-1].uid!==msg.uid ); //for style purposes
                            const is_last = i===(arr.length-1); //for style purposes
                            const is_sent = (msg.uid === props.auth.currentUser.uid); //Either it's sent or received
                            return <ChatMessage key={msg.id} msg={msg} sent={is_sent} delete={() => setDeleteMsg(msg)} last={is_last}/>;
                        })
                        : <Loader />
                }
                <div ref={bottom}></div>
            </div>
            <MessageForm auth={props.auth} messagesRef={messagesRef}/>
        </div>
    );
};

export default ChatRoom;