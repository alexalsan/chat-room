import React, { useState } from 'react';
import classes from './MessageForm.module.css';

import firebase from 'firebase';

const MessageForm = (props) => {
    const [formValue, setValue] = useState("");

    const sendMessage = async (event) => {
        event.preventDefault(); //To stop the page from refreshing

        const {uid, photoURL, displayName} = props.auth.currentUser;
        await props.messagesRef.add({
            text: formValue,
            createdAt: firebase.firestore.FieldValue.serverTimestamp(),
            uid,
            photoURL,
            displayName
        });
        setValue("");
    }

    return (
        <form className={classes.MsgForm} onSubmit={sendMessage}>
            <input value={formValue} placeholder="Write your message here..." onChange={(e) => setValue(e.target.value)}/>
            <button type="submit" disabled={formValue===""} className={classes.SendButton}>Send</button>
        </form>
    );
};

export default MessageForm;