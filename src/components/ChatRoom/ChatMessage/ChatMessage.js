import React from 'react';
import classes from './ChatMessage.module.css';
import { formatDate, formatName } from '../../../utils';
import trash from '../../../assets/cross.png';

const ChatMessage = (props) => {
    const {text, createdAt, photoURL, displayName} = props.msg;
    const date_string = formatDate(createdAt);
    const name_string = formatName(displayName, props.sent);

    return (
        <div className={[classes.Message, props.sent ? classes.Sent : classes.Received, props.last ? classes.NewMsg : ""].join(" ")}>
            <img className={classes.UserImage} alt="" src={photoURL} />
            <div className={classes.ArrowLeft}></div>
            <div className={classes.MsgText}>
                <p>{ name_string } • { date_string }</p>
                <h3>{text}</h3>
                { props.sent && <img alt="delete message" src={trash} className={classes.DeleteIcon} onClick={props.delete}/> }
            </div>
            <div className={classes.ArrowRight}></div>
        </div>
    );
};

export default ChatMessage;