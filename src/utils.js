const monthNames = ["Jan.", "Feb.", "Mar.", "Apr.", "May", "June",
  "July", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."];

// const monthNames = ["January", "February", "March", "April", "May", "June",
//   "July", "August", "September", "October", "November", "December"];

export const formatDate = (fb_timestamp) => {
    if (!fb_timestamp) { return null; }

    const old_date = fb_timestamp.toDate(); //We need to convert the Firebase Timestamp format
    const current_date = new Date();
    const time_string = `${old_date.getHours()<10 ? "0":""}${old_date.getHours()}:${old_date.getMinutes()<10 ? "0":""}${old_date.getMinutes()}`;

    if (old_date.getFullYear() === current_date.getFullYear() &&
        old_date.getMonth() === current_date.getMonth()) {
      
        //Is it today?
        if (old_date.getDate() === current_date.getDate()) {
            return (`Today, ${time_string}`);
        }

        //Is it yesterday?
        else if (old_date.getDate() === current_date.getDate() - 1) {
            return (`Yesterday, ${time_string}`);
        }
    }
    
    //We only show the year if it's not the current one
    const year_string = (old_date.getFullYear() === current_date.getFullYear()) ? "" : " "+old_date.getFullYear(); 

    return (`${old_date.getDate()} ${monthNames[old_date.getMonth()]}${year_string}, ${time_string}`);
}

export const formatName = (old_name, is_sent) => {
    if (!old_name){ return null; }
    return ( is_sent ? "Me" : old_name.replace(/ .*/,'') );
}

export const formatDeletedMsg = (old_text) => {
    return (old_text.length > 10) ? `"${old_text.substring(0,10)}..."` : `"${old_text}"`;
}