import React from 'react';
import ChatRoom from '../../components/ChatRoom/ChatRoom';
import SignIn from '../../components/SignIn/SignIn';

import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/auth';

import { useAuthState } from 'react-firebase-hooks/auth';

// Initialize Firebase
firebase.initializeApp({
  apiKey: "AIzaSyCwNw2cRU8QdZwngQ8L63Eofr85qdb3wPc",
  authDomain: "chatroom-personal-project.firebaseapp.com",
  databaseURL: "https://chatroom-personal-project.firebaseio.com",
  projectId: "chatroom-personal-project",
  storageBucket: "chatroom-personal-project.appspot.com",
  messagingSenderId: "113460598981",
  appId: "1:113460598981:web:ac89ec9b9c7c3921de38ac"
});

const auth = firebase.auth();
const firestore = firebase.firestore();

const Layout = (props) => {
  const [user] = useAuthState(auth);

  return (
    user ? <ChatRoom auth={auth} firestore={firestore}/> : <SignIn auth={auth}/>
  );
};

export default Layout;