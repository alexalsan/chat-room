# Chatroom (2020)
## Check first draft on [https://chatroom-personal-project.web.app/](https://chatroom-personal-project.web.app/)

It is a small personal project I implemented to get familiar with React. It is a small chat where you can post messages and see what other people posted as well. The project was born as a challenge of implementing a functional web app in a few days.

It uses a variety of React & JavaScript features: dynamic elements, asynchronous programming,... The database used to store the messages is Firebase, due to its short learning curve and its compatibility with React.

There are a lot of small details I took care of to make it user-friendly and comfortable. There is special focus on responsive design. In addition, Firebase's authentication system via Google accounts make it very secure to use, and assures that you can only delete or modify your own messages.

## Technologies used
* React
* JavaScript
* CSS
* HTML
* Firebase (database)
* Interface design
* Responsive design